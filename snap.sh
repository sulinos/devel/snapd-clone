#!/bin/bash
originaldir=$(pwd)
snapdir="/snap/"
cachedir="/var/snap"
jsoncontent=""
args=""
shellmode="disable"
for i in $*
do
	if [ "$i" == "--user" ]; then
		export snapdir=$HOME/.snap
		export cachedir=$HOME/.snap/cache
	elif [ "$i" == "--shell" ]; then
		export shellmode="shell"
	elif [ "$i" == "--exec" ]; then
		export shellmode="exec"
	fi
done
for i in ${@:3}
do
	if [ "$i" != "--user" ] &&[ "$i" != "--shell" ] &&[ "$i" != "--exec" ]; then
		args+="$i "
	fi
done

get_json(){
	if [ ! -f /$cachedir/$1.json ] ; then
		jsoncontent=$(curl -H 'Snap-Device-Series: 16' "http://api.snapcraft.io/v2/snaps/info/$1" 2>/dev/null)
		echo "$jsoncontent" > /$cachedir/$1.json
	else
		jsoncontent=$(cat /$cachedir/$1.json)
	fi
}
write_json(){
	echo $jsoncontent | python3 -m json.tool
}
get_key(){
	echo $jsoncontent | python3 -m json.tool | grep "\"$1\"" | head -n 1 | sed "s/.*$1\"://" | sed "s/\"//g"
}
snap_install(){
	get_json $1
	if [ "$(get_key name)" == "" ] ; then
		echo "Package $1 not exists."
		rm -f /$cachedir/$1.json
		exit 1
	fi
	if [ ! -f "$(get_key name).snap" ] ; then
		echo $(get_key url)
		wget -c $(get_key url) -O "$(get_key name).snap"
	fi
	if [ ! -d /$snapdir/$(get_key package_name)/ ] ; then
		unsquashfs $(get_key name).snap
		mkdir -p /$snapdir/$(get_key package_name)/ &>/dev/null
		mv squashfs-root/* /$snapdir/$(get_key package_name)
		rm -rf squashfs-root
	fi
	icon_url=$(get_key "icon_url")
	if [ "$icon_url" != "" ] ; then
		if [ "$snapdir" == "/snap/" ] ; then
			curl "$icon_url" > /usr/share/icons/snap-$(get_key "name")
			write_desktop "$(get_key package_name)" > /usr/share/applications/snap-$(get_key name).desktop
			chmod 755 /usr/share/applications/snap-$(get_key name).desktop
		else
			mkdir -p $HOME/.local/share/applications &>/dev/null
			mkdir -p $HOME/.icons &>/dev/null
			wget -q --show-progress -c "$icon_url" -O $HOME/.icons/snap-$(get_key "name")
			write_desktop "$(get_key package_name)" > $HOME/.local/share/applications/snap-$(get_key name).desktop
			chmod 755 $HOME/.local/share/applications/snap-$(get_key name).desktop
		fi
	fi
	if [ "$1" == "core" ] ; then
		echo "Core fixes processing"
		ln -s /snap/core /snap/core/current
	fi
}

snap_remove(){
	get_json $1
	if [ "$(get_key package_name)" == "" ] ; then
		echo "Package not detected."
		exit 1
	fi	
	if [ -d /$snapdir/$(get_key package_name) ] ; then
		rm -rf /$snapdir/$1
	else
		echo "Package $1 not exists."
		exit 1
	fi
	if [ "$snapdir" == "/snap" ] ; then
		[ -f /usr/share/applications/snap-$(get_key name).desktop ] && rm -f /usr/share/applications/snap-$(get_key name).desktop
		[ -f /usr/share/icons/snap-$(get_key name) ] && rm -f /usr/share/icons/snap-$(get_key name)
	else
		[ -f $HOME/.local/share/applications/snap-$(get_key name).desktop ] && rm -f $HOME/.local/share/applications/snap-$(get_key name).desktop
		[ -f $HOME/.local/share/icons/snap-$(get_key name) ] && rm -f $HOME/.local/share/icons/snap-$(get_key name)
	
	fi
}

write_desktop(){
	if [ "$snapdir" == "/snap/" ] ; then
		icon_name=/usr/share/icons/snap-$(get_key "name")
	else
		icon_name=$HOME/.icons/snap-$(get_key "name")
	fi
	echo "[Desktop Entry]"
	echo "Version=1.0"
	echo "Name=$(get_key package_name) (snap)"
	echo "Comment=$(get_key summary)"
	echo "Categories=Utility;"
	if [ "$snapdir" == "/snap/" ] ; then
		echo "Exec=snap run $(get_key package_name)"
	else
		echo "Exec=snap run $(get_key package_name) --user"
	fi
	echo "Icon=$icon_name"
	echo "Terminal=false"
	echo "Type=Application"
	echo "X-GNOME-UsesNotifications=true"
}

snap_info(){
	while read line
	do
		get_json $line
		echo "$(get_key package_name) : $(get_key summary)"
	done
}
mkdir -p $cachedir &>/dev/null
cd $cachedir
if [ "$1" == "run" ] ; then
	name=$2
	if [ ! -d /$snapdir/$2 ] ; then
		echo "Package $2 not installed."
		exit 0;
	elif [ ! -d /snap/core ] ; 
	then
		echo "Core not found."
		echo "You should run \e[;1m\"snap install core\"\e[;0m as root"
	fi
	[ "$(uname -m)" == "x86_64" ] && export SNAP_ARCH="amd64"
	export SNAP="/$snapdir/$name"
	mkdir "$HOME/.snap/$name" &>/dev/null
	export SNAP_USER_DATA="$HOME/.snap/$name"
	export SNAP_USER_COMMON="$HOME/.snap/$name"
	export XDG_CONFIG_HOME="$HOME/.snap/$name"
	export XDG_RUNTIME_DIR="$HOME/.snap/$name"
	
	
	export XDG_DATA_DIRS=$XDG_DATA_DIRS:/snap/core/usr/share
	export XDG_CONFIG_DIRS=$XDG_CONFIG_DIRS:/snap/core/etc/xdg
	for dirs in $(ls /$snapdir/ | grep -v core)
	do
		export XDG_DATA_DIRS=$XDG_DATA_DIRS:/$snapdir/$dirs/usr/share
		export XDG_CONFIG_DIRS=$XDG_CONFIG_DIRS:/$snapdir/$dirs/etc/xdg
	done

	export PATH=$PATH:/snap/core/bin:/snap/core/sbin:/snap/core/usr/bin:/snap/core/usr/sbin
	
	for dirs in $(ls /$snapdir/ | grep -v core)
	do
		export PATH=$PATH$( find /$snapdir/$dirs/bin -type d -printf ":%p" 2>/dev/null)
		export PATH=$PATH$( find /$snapdir/$dirs/usr/bin -type d -printf ":%p" 2>/dev/null)
	done

	#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH$( find /snap/core/lib -type d -printf ":%p" 2>/dev/null)
	#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH$( find /snap/core/usr/lib -type d -printf ":%p" 2>/dev/null)

	for dirs in $(ls /$snapdir/ | grep -v core)
	do
		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH$( find /$snapdir/$dirs/lib -type d -printf ":%p" 2>/dev/null)
		export LD_LIBRARY_PATH=$LD_LIBRARY_PATH$( find /$snapdir/$dirs/usr/lib -type d -printf ":%p" 2>/dev/null)
	done
	export HOME="$HOME/.snap/$name"
	export USER=$name
	export PS1="\e[32;1msnap@$name\e[;0m:\e[34;1m\W\e[;0m$ "
	export PS2=">"
	if [ "$shellmode" == "disable" ] ; then
		wrapper=$(ls /$snapdir/$name/command*wrapper 2>/dev/null | tail -n 1)
		if [ -f "/$snapdir/$name/command-$name-wrapper" ] ; then
			exec /$snapdir/$name/command-$name-wrapper $args
		elif [ -f "$wrapper" ] ; then
			exec $wrapper $args
		elif [ -f  /$snapdir/$name/bin/desktop-launch ] ; then
			 /$snapdir/$name/bin/desktop-launch	/$snapdir/$name/bin/$name
		else
			$name $args
		fi
	elif [ "$shellmode" == "shell" ] ; then
		cd $HOME
		$SHELL --noprofile --norc $args
	elif [ "$shellmode" == "exec" ] ; then
		$args
	fi
elif [ "$1" == "+" ] ; then
	echo "self install $0 => $DESTDIR/usr/bin/snap"
	cat $originaldir/$0 > $DESTDIR/usr/bin/snap
	chmod +x $DESTDIR/usr/bin/snap
elif [ "$1" == "remove" ] ; then
	for i in ${@:2}
	do
		[ "$i" == "--user" ]  || snap_remove $i
	done
elif [ "$1" == "install" ] ; then
	if [ ! -d /snap/core ] ; 
	then
		if [ "$snapdir" == "/snap/" ] ; then
			snap_install core
		else
			echo "Snap core not found."
			exit 1
		fi
	fi	
	for i in ${@:2}
	do
		[ "$i" == "--user" ] || snap_install $i
	done
elif [ "$1" == "info" ] ; then
	get_json $2
	if [ "$(get_key name)" == "" ] ; then
		echo "Package $2 not exists."
		rm -f /$cachedir/$2.json
		exit 1
	fi
	write_json
elif [ "$1" == "clear" ] ; then
	rm -rf /$cachedir/*
elif [ "$1" == "list" ] ; then
	ls /$snapdir | grep -v cache | snap_info
else
	echo "Usege: snap [install/remove/info/list/clear] [--user/--shell/--exec] [package names]"
	echo "    install    : install package"
	echo "    remove     : remove package"
	echo "    info       : get info a package"
	echo "    list       : list installed packages"
	echo "    clear      : remove downloaded files"
	echo "    --user     : rootless run mode. (change snapdir: $HOME/.snap/)"
	echo "    --shell    : start shell with snap environment"
	echo "    --exec     : execute command with snap environment"
	
fi
