# dependencies
```
curl
wget
bash
python3 & python3 json.tool module
```

# for install

`bash snap.sh +`

# usage
```
Usege: snap [install/remove/info/list/clear] [--user] [package names]
    install    : install package
    remove     : remove package
    info       : get info a package
    list       : list installed packages
    clear      : remove downloaded files
```
